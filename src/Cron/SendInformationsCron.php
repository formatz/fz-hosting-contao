<?php

namespace Formatz\FzHostingsBundle\Cron;

use Contao\CoreBundle\DependencyInjection\Attribute\AsCronJob;
use Contao\CoreBundle\Exception\CronExecutionSkippedException;
use Formatz\FzHostingsBundle\FzHostings\HostInformationsUpdater;

class SendInformationsCron
{
    public function __construct(private HostInformationsUpdater $versionsManager)
    {
    }

    #[AsCronJob('weekly')]
    public function onWeekly(): void
    {
        $response = $this->versionsManager->sendCurrentInformations();

        if (empty($response)) {
            throw new CronExecutionSkippedException();
        }
    }
}
