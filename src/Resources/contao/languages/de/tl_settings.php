<?php

$GLOBALS['TL_LANG']['tl_settings']['fzhostings_legend'] = 'FZ Hostings';
$GLOBALS['TL_LANG']['tl_settings']['fzHostingsEndpoint'] = ['FZ Hostings API-Endpunkt', ''];
$GLOBALS['TL_LANG']['tl_settings']['fzHostingsKey'] = ['FZ Hostings API-Schlüssel', 'Finden Sie den Schlüssel auf der Seite der Unterkunft von FZ Hostings.'];
$GLOBALS['TL_LANG']['tl_settings']['fzHostingsVersionsSent'] = 'Die Versionen wurden an den Hoster gesendet.';
$GLOBALS['TL_LANG']['tl_settings']['fzHostingsVersionsNotSent'] = 'Die Versionen wurden nicht an den Hoster gesendet.';
