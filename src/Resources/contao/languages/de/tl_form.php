<?php

$GLOBALS['TL_LANG']['tl_form']['sendFormDataViaEmailUsingFzHostings'] = [
    'Send via email mit FZ Hostings (mit Anti-Spam)',
    'Senden Sie die Formulardaten per E-Mail über FZ Hostings. Darüber hinaus wird der Inhalt überprüft (Anti-Spam).',
];
