<?php

Contao\System::loadLanguageFile('tl_form');

$GLOBALS['TL_LANG']['send-form-data-via-email-using-fz-hostings']['errorWhileSendingFormData'] = 'Beim Senden des Formulars ist ein Fehler aufgetreten';
$GLOBALS['TL_LANG']['send-form-data-via-email-using-fz-hostings']['cannotActiveBothOptions'] = 'Die Option "'.
    $GLOBALS['TL_LANG']['tl_form']['sendViaEmail'][0]
    .'" wurde deaktiviert, da die Option "'.
    $GLOBALS['TL_LANG']['tl_form']['sendFormDataViaEmailUsingFzHostings'][0]
    .'" aktiviert ist.';
