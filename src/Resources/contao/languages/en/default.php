<?php

Contao\System::loadLanguageFile('tl_form');

$GLOBALS['TL_LANG']['send-form-data-via-email-using-fz-hostings']['errorWhileSendingFormData'] = 'An error occurred while sending the form';
$GLOBALS['TL_LANG']['send-form-data-via-email-using-fz-hostings']['cannotActiveBothOptions'] = 'The option "'.
    $GLOBALS['TL_LANG']['tl_form']['sendViaEmail'][0]
    .'" is disabled because the option "'.
    $GLOBALS['TL_LANG']['tl_form']['sendFormDataViaEmailUsingFzHostings'][0]
    .'" is enabled.';
