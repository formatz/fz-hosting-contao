<?php

$GLOBALS['TL_LANG']['tl_form']['sendFormDataViaEmailUsingFzHostings'] = [
    'Send via email using FZ Hostings (with anti-spam)',
    'Send the form data via email using FZ Hostings. In addition, it will check the content (anti-spam).',
];
