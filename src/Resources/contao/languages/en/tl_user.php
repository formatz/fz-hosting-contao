<?php

$GLOBALS['TL_LANG']['tl_user']['fzHostingsNotificationMessage'] = ['Notification message', 'Displayed when logging into the backend.'];
$GLOBALS['TL_LANG']['tl_user']['fzHostingsNotificationMessageLevel'] = ['Notification level', 'Notification message level'];
