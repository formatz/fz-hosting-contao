<?php

$GLOBALS['TL_LANG']['tl_settings']['fzhostings_legend'] = 'FZ Hostings';
$GLOBALS['TL_LANG']['tl_settings']['fzHostingsEndpoint'] = ['FZ Hostings API Endpoint', 'Find the key on FZ Hostings host\'s page.'];
$GLOBALS['TL_LANG']['tl_settings']['fzHostingsKey'] = ['FZ Hostings API Key', 'Find the key on FZ Hostings host\'s page.'];
$GLOBALS['TL_LANG']['tl_settings']['fzHostingsVersionsSent'] = 'The versions have been sent to the host.';
$GLOBALS['TL_LANG']['tl_settings']['fzHostingsVersionsNotSent'] = 'The versions have not been sent to the host.';
