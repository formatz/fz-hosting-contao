<?php

$GLOBALS['TL_LANG']['tl_settings']['fzhostings_legend'] = 'FZ Hostings';
$GLOBALS['TL_LANG']['tl_settings']['fzHostingsEndpoint'] = ['Point d\'accès à l\'API de FZ Hostings', ''];
$GLOBALS['TL_LANG']['tl_settings']['fzHostingsKey'] = ['Clé de l\'API de FZ Hostings', 'Retrouvez la clé sur la page de l\'hébergement de FZ Hostings.'];
$GLOBALS['TL_LANG']['tl_settings']['fzHostingsVersionsSent'] = 'Les versions ont été envoyées à l\'hébergeur.';
$GLOBALS['TL_LANG']['tl_settings']['fzHostingsVersionsNotSent'] = 'Les versions n\'ont pas été envoyées à l\'hébergeur.';
