<?php

$GLOBALS['TL_LANG']['tl_form']['sendFormDataViaEmailUsingFzHostings'] = [
    'Envoyer par email via FZ Hostings (avec anti-spam)',
    'Envoyer par mail les données du formulaire via FZ Hostings. De plus, il vérifiera le contenu (anti-spam).',
];
