<?php

$GLOBALS['TL_LANG']['tl_user']['fzHostingsNotificationMessage'] = ['Message de notification', 'Affiché lors de la connexion au backend'];
$GLOBALS['TL_LANG']['tl_user']['fzHostingsNotificationMessageLevel'] = ['Niveau de la notification', 'Niveau du message de notification'];
