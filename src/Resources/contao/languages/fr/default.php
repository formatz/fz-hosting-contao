<?php

Contao\System::loadLanguageFile('tl_form');

$GLOBALS['TL_LANG']['send-form-data-via-email-using-fz-hostings']['errorWhileSendingFormData'] = 'Une erreur est survenue lors de l\'envoi du formulaire';
$GLOBALS['TL_LANG']['send-form-data-via-email-using-fz-hostings']['cannotActiveBothOptions'] = 'L\'option "'.
    $GLOBALS['TL_LANG']['tl_form']['sendViaEmail'][0]
    .'" a été désactivée car l\'option "'.
    $GLOBALS['TL_LANG']['tl_form']['sendFormDataViaEmailUsingFzHostings'][0]
    .'" est activée.';
