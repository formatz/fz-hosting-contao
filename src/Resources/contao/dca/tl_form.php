<?php

use Contao\CoreBundle\DataContainer\PaletteManipulator;

$GLOBALS['TL_DCA']['tl_form']['fields']['sendFormDataViaEmailUsingFzHostings'] = [
    'exclude' => true,
    'filter' => true,
    'inputType' => 'checkbox',
    'eval' => ['submitOnChange' => true, 'tl_class' => 'clr'],
    'sql' => "char(1) NOT NULL default ''",
];

$GLOBALS['TL_DCA']['tl_form']['subpalettes']['sendFormDataViaEmailUsingFzHostings'] = 'recipient';
$GLOBALS['TL_DCA']['tl_form']['palettes']['__selector__'][] = 'sendFormDataViaEmailUsingFzHostings';

PaletteManipulator::create()
    ->addField('sendFormDataViaEmailUsingFzHostings', 'email_legend', PaletteManipulator::POSITION_APPEND)
    ->applyToPalette('default', 'tl_form');
