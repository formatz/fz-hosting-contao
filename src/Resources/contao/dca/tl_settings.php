<?php

use Contao\CoreBundle\DataContainer\PaletteManipulator;

$GLOBALS['TL_DCA']['tl_settings']['fields']['fzHostingsEndpoint'] = [
    'inputType' => 'text',
    'eval' => ['mandatory' => true, 'decodeEntities' => true, 'tl_class' => 'w50', 'placeholder' => 'https://hostings.format-z.ch'],
];

$GLOBALS['TL_DCA']['tl_settings']['fields']['fzHostingsKey'] = [
    'inputType' => 'text',
    'eval' => ['mandatory' => true, 'decodeEntities' => true, 'tl_class' => 'w50'],
];

PaletteManipulator::create()
    ->addLegend('fzhostings_legend', 'global_legend')
    ->addField('fzHostingsEndpoint', 'fzhostings_legend', PaletteManipulator::POSITION_APPEND)
    ->addField('fzHostingsKey', 'fzhostings_legend', PaletteManipulator::POSITION_APPEND)
    ->applyToPalette('default', 'tl_settings');
