<?php

$GLOBALS['TL_DCA']['tl_form_submission'] = [
    'config' => [
        'dataContainer' => DC_Table::class,
        'sql' => [
            'keys' => [
                'id' => 'primary',
            ],
        ],
    ],
    'fields' => [
        'id' => [
            'sql' => 'int(10) unsigned NOT NULL auto_increment',
        ],
        'tstamp' => [
            'sql' => 'int(10) unsigned NOT NULL default 0',
        ],
        'form_id' => [
            'sql' => 'int(10) unsigned NOT NULL default 0',
        ],
        'data' => [
            'sql' => 'blob NULL',
        ],
    ],
];
