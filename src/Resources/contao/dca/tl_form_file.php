<?php

$GLOBALS['TL_DCA']['tl_form_file'] = [
    'config' => [
        'dataContainer' => DC_Table::class,
        'sql' => [
            'keys' => [
                'id' => 'primary',
            ],
        ],
    ],
    'fields' => [
        'id' => [
            'sql' => 'int(10) unsigned NOT NULL auto_increment',
        ],
        'tstamp' => [
            'sql' => 'int(10) unsigned NOT NULL default 0',
        ],
        'form_id' => [
            'sql' => 'int(10) unsigned NOT NULL default 0',
        ],
        'file' => [
            'sql' => 'text NOT NULL default \'\'',
        ],
        'originalName' => [
            'sql' => 'varchar(255) NOT NULL default \'\'',
        ],
        'name' => [
            'sql' => 'varchar(255) NOT NULL default \'\'',
        ],
        'expirationDate' => [
            'sql' => 'int(10) NOT NULL default 0',
        ],
    ],
];
