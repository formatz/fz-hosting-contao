<?php

use Contao\CoreBundle\DataContainer\PaletteManipulator;

$GLOBALS['TL_DCA']['tl_user']['fields']['fzHostingsNotificationMessage'] = [
    'inputType' => 'textarea',
    'eval' => ['mandatory' => false, 'helpwizard' => true, 'tl_class' => 'clr'],
    'explanation' => 'insertTags',
    'sql' => 'mediumtext NULL',
];

$GLOBALS['TL_DCA']['tl_user']['fields']['fzHostingsNotificationMessageLevel'] = [
    'inputType' => 'select',
    'options' => [
        'TL_INFO' => 'Info',
        'TL_CONFIRM' => 'Confirm',
        'TL_ERROR' => 'Error',
        'TL_NEW' => 'New',
    ],
    'eval' => ['mandatory' => false, 'chosen' => true, 'decodeEntities' => true, 'tl_class' => 'clr'],
    'sql' => "varchar(255) NOT NULL default 'TL_INFO'",
];

PaletteManipulator::create()
    ->addField('fzHostingsNotificationMessage', 'backend_legend', PaletteManipulator::POSITION_APPEND)
    ->addField('fzHostingsNotificationMessageLevel', 'backend_legend', PaletteManipulator::POSITION_APPEND)
    ->applyToPalette('default', 'tl_user')
    ->applyToPalette('admin', 'tl_user')
    ->applyToPalette('group', 'tl_user')
    ->applyToPalette('extend', 'tl_user')
    ->applyToPalette('custom', 'tl_user')
    ->applyToPalette('login', 'tl_user')
;
