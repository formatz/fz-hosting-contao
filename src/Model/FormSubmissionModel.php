<?php

namespace Formatz\FzHostingsBundle\Model;

use Contao\Model;

/**
 * Reads and writes form submission.
 *
 * @property string|int $id
 * @property int        $tstamp
 * @property string     $form_id
 * @property string     $data
 *
 * @method static FormSubmissionModel|null findById($id, array $opt = array())
 */
class FormSubmissionModel extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    protected static $strTable = 'tl_form_submission';

    public function __toString()
    {
        return self::class.'|'.$this->id;
    }
}

class_alias(FormSubmissionModel::class, 'FormSubmissionModel');
