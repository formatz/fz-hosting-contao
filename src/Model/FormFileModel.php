<?php

namespace Formatz\FzHostingsBundle\Model;

use Contao\Model;

/**
 * Reads and writes form file.
 *
 * @property string|int $id
 * @property int        $tstamp
 * @property string     $form_id
 * @property string     $file
 * @property string     $originalName
 * @property string     $name
 * @property \DateTime  $expirationDate
 *
 * @method static FormFileModel|null findById($id, array $opt = array())
 */
class FormFileModel extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    protected static $strTable = 'tl_form_file';

    public function __toString()
    {
        return self::class.'|'.$this->id;
    }

    public static function deleteExpiredFiles(): void
    {
        $objFiles = self::findBy(['expirationDate<?'], [time()]);

        if (null === $objFiles) {
            return;
        }

        foreach ($objFiles as $formFile) {
            if (file_exists($formFile->file)) {
                unlink($formFile->file);
            }

            $formFile->delete();
        }
    }
}

class_alias(FormFileModel::class, 'FormFileModel');
