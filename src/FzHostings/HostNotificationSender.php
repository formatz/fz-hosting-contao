<?php

namespace Formatz\FzHostingsBundle\FzHostings;

use Contao\CoreBundle\Monolog\ContaoContext;
use Contao\User;
use Psr\Log\LoggerInterface;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class HostNotificationSender
{
    public const API_ENDPOINT_USER_LOG = '/api/v2/user-log';
    public const API_ENDPOINT_SEND_FORM_DATA_VIA_EMAIL = '/api/v2/send-form-data-via-email';

    public function __construct(private HttpClientInterface $httpClient, private LoggerInterface $logger, private PropertyAccessorInterface $propertyAccessor)
    {
    }

    private function sendRequest(string $endpoint, array $data): ?ResponseInterface
    {
        if (empty($GLOBALS['TL_CONFIG']['fzHostingsEndpoint']) || empty($GLOBALS['TL_CONFIG']['fzHostingsKey'])) {
            return null;
        }

        try {
            $response = $this->httpClient->request(
                'POST',
                rtrim($GLOBALS['TL_CONFIG']['fzHostingsEndpoint'], '/').$endpoint,
                [
                    'headers' => [
                        'FZ-HOSTING-AUTH-TOKEN' => $GLOBALS['TL_CONFIG']['fzHostingsKey'],
                    ],
                    'json' => $data,
                ]
            );

            if (($statusCode = $response->getStatusCode()) >= 300) {
                $message = 401 === $statusCode ? 'Server returned a 401 Unauthorized when sending request.' : $response->getContent(false);
                $context = ['contao' => new ContaoContext(__METHOD__, ContaoContext::ERROR)];

                // Because API return 400 when form data is invalid (spam, etc.)
                if (400 !== $response->getStatusCode()) {
                    $this->logger->error($message, $context);
                } else {
                    $this->logger->warning($message, $context);
                }

                return null;
            }
        } catch (\Exception $e) {
            $this->logger->error(
                $e->getMessage(),
                ['contao' => new ContaoContext(__METHOD__, ContaoContext::ERROR)]
            );

            return null;
        }

        return $response;
    }

    public function sendNotification(User $user): ?ResponseInterface
    {
        $isAdmin = false;

        if ($this->propertyAccessor->isReadable($user, 'admin')) {
            $isAdmin = $this->propertyAccessor->getValue($user, 'admin');
        }

        $data = [
            'user' => [
                'id' => $user->id,
                'username' => $user->username,
                'name' => $user->name,
                'email' => $user->email,
                'isAdmin' => $isAdmin,
            ],
            'userAgent' => $_SERVER['HTTP_USER_AGENT'],
            'ip' => $_SERVER['REMOTE_ADDR'],
        ];

        return $this->sendRequest(self::API_ENDPOINT_USER_LOG, $data);
    }

    public function sendFormDataViaEmailUsingFzHostings(array $content): ?ResponseInterface
    {
        return $this->sendRequest(self::API_ENDPOINT_SEND_FORM_DATA_VIA_EMAIL, $content);
    }
}
