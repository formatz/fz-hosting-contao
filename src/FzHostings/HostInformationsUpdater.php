<?php

namespace Formatz\FzHostingsBundle\FzHostings;

use Composer\InstalledVersions;
use Contao\CoreBundle\Monolog\ContaoContext;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class HostInformationsUpdater
{
    public const API_ENDPOINT_LOG = '/api/v2/log';

    public function __construct(private HttpClientInterface $httpClient, private LoggerInterface $logger)
    {
    }

    public function sendCurrentInformations(): ?ResponseInterface
    {
        if (empty($GLOBALS['TL_CONFIG']['fzHostingsEndpoint']) || empty($GLOBALS['TL_CONFIG']['fzHostingsKey'])) {
            return null;
        }

        $versions = [
            'php' => PHP_VERSION,
        ];

        $packages = [
            'formatz/fz-hosting-contao-bundle' => 'fz-hosting-contao',
            'symfony/console' => 'symfony',
            'contao/core-bundle' => 'contao',
            'isotope/isotope-core' => 'isotope',
            'formatz/contao-headless-cms-bundle' => 'contao-headless-cms',
            'dieschittigs/contao-content-api' => 'contao-headless-cms',
        ];

        foreach ($packages as $packageName => $name) {
            $this->addVersionIfInstalled($versions, $packageName, $name);
        }

        $smtpUrl = $smtpPassword = null;

        if (!empty($_ENV['MAILER_DSN'])) {
            $smtpUrl = urldecode($_ENV['MAILER_DSN']);
            $smtpPassword = parse_url($smtpUrl, PHP_URL_PASS);
        } elseif (!empty($_ENV['MAILER_URL'])) {
            $smtpUrl = urldecode($_ENV['MAILER_URL']);
            $queryString = parse_url($smtpUrl, PHP_URL_QUERY);
            parse_str($queryString, $query);

            // password could exists on query parameter "password".
            // Example : smtp://mail.infomaniak.ch:587?encryption=tls&auth_mode=login&username=smtp@formatlabs.ch&password=***
            $smtpPassword = $query['password'] ?? null;
        }

        $smtp = $smtpUrl ?: '';

        if (!empty($smtpPassword)) {
            $smtp = str_replace($smtpPassword, '***', $smtp);
        }

        try {
            $response = $this->httpClient->request(
                'POST',
                rtrim($GLOBALS['TL_CONFIG']['fzHostingsEndpoint'], '/').self::API_ENDPOINT_LOG,
                [
                    'headers' => [
                        'FZ-HOSTING-AUTH-TOKEN' => $GLOBALS['TL_CONFIG']['fzHostingsKey'],
                    ],
                    'json' => [
                        'versions' => $versions,
                        'smtp' => $smtp,
                    ],
                ]
            );

            if (($statusCode = $response->getStatusCode()) >= 300) {
                $this->logger->error(
                    401 === $statusCode ? 'Server returned a 401 Unauthorized when sending current packages versions.' : $response->getContent(false),
                    ['contao' => new ContaoContext(__METHOD__, ContaoContext::ERROR)]
                );

                return null;
            }
        } catch (\Exception $e) {
            $this->logger->error(
                $e->getMessage(),
                ['contao' => new ContaoContext(__METHOD__, ContaoContext::ERROR)]
            );

            return null;
        }

        return $response;
    }

    public function addVersionIfInstalled(array &$versions, string $packageName, string $name): void
    {
        if (InstalledVersions::isInstalled($packageName)) {
            $versions[$name] = InstalledVersions::getVersion($packageName);
        }
    }
}
