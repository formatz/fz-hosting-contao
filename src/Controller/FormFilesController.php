<?php

namespace Formatz\FzHostingsBundle\Controller;

use Contao\CoreBundle\Controller\AbstractController;
use Formatz\FzHostingsBundle\Model\FormFileModel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class FormFilesController extends AbstractController
{
    #[Route('/form-files/{name}', name: 'form_files')]
    public function getByName(Request $request, string $name)
    {
        $this->initializeContaoFramework();
        FormFileModel::deleteExpiredFiles();

        $formFile = FormFileModel::findOneBy('name', $name);

        if (null === $formFile) {
            throw new NotFoundHttpException('File not found');
        }

        $file = new \SplFileInfo($formFile->file);

        if (!$file->isFile()) {
            $formFile->delete();

            throw new NotFoundHttpException('File not found');
        }

        if ($formFile->expirationDate < time()) {
            throw new NotFoundHttpException('File expired');
        }

        return new BinaryFileResponse($file);
    }
}
