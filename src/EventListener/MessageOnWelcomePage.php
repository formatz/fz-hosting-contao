<?php

namespace Formatz\FzHostingsBundle\EventListener;

use Contao\CoreBundle\DependencyInjection\Attribute\AsHook;
use Contao\Message;
use Contao\StringUtil;
use Contao\System;

#[AsHook('parseBackendTemplate')]
class MessageOnWelcomePage
{
    public function __invoke(string $buffer, string $template): string
    {
        if ('be_welcome' === $template) {
            // get the current user
            $user = System::getContainer()->get('security.token_storage')->getToken()->getUser();

            if (
                !empty($message = $user->fzHostingsNotificationMessage)
                && !empty($level = $user->fzHostingsNotificationMessageLevel)
            ) {
                // add the HTML message to the welcome screen
                $buffer = '<div class="'.strtolower($level).'">'.StringUtil::restoreBasicEntities($message).'</div>'.$buffer;
            }
        }

        return $buffer;
    }
}
