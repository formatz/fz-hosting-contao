<?php

namespace Formatz\FzHostingsBundle\EventListener;

use Contao\CoreBundle\DependencyInjection\Attribute\AsCallback;
use Contao\DataContainer;
use Contao\Message;
use Formatz\FzHostingsBundle\FzHostings\HostInformationsUpdater;

class SendInformationsOnSubmit
{
    public function __construct(private HostInformationsUpdater $versionsManager)
    {
    }

    #[AsCallback(table: 'tl_settings', target: 'config.onsubmit')]
    public function onSubmit(DataContainer $dc): void
    {
        // Send the current versions to the host
        if ($this->versionsManager->sendCurrentInformations()) {
            // Add a confirmation message to the backend if the versions have been sent
            Message::addInfo($GLOBALS['TL_LANG']['tl_settings']['fzHostingsVersionsSent']);
        } else {
            // Add an error message to the backend if the versions have not been sent
            Message::addError($GLOBALS['TL_LANG']['tl_settings']['fzHostingsVersionsNotSent']);
        }
    }
}
