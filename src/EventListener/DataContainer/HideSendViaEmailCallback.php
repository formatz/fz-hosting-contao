<?php

namespace Formatz\FzHostingsBundle\EventListener\DataContainer;

use Contao\CoreBundle\DependencyInjection\Attribute\AsCallback;
use Contao\DataContainer;
use Contao\FormModel;
use Contao\Message;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

#[AsCallback(table: 'tl_form', target: 'config.onsubmit')]
class HideSendViaEmailCallback
{
    public function __construct(private RequestStack $requestStack, private PropertyAccessorInterface $propertyAccessor)
    {
    }

    public function __invoke(?DataContainer $dc = null): void
    {
        if (null === $dc || !$dc->id || 'edit' !== $this->requestStack->getCurrentRequest()->query->get('act')) {
            return;
        }

        $element = FormModel::findById($dc->id);

        if (null === $element) {
            return;
        }

        if (!$this->propertyAccessor->isReadable($dc, 'activeRecord')) {
            return;
        }

        $activeRecord = $this->propertyAccessor->getValue($dc, 'activeRecord');

        if (null === $activeRecord) {
            return;
        }

        if (!is_array($activeRecord)) {
            $activeRecord = $activeRecord->row();
        }

        if ($activeRecord['sendFormDataViaEmailUsingFzHostings'] && $activeRecord['sendViaEmail']) {
            $element->sendViaEmail = false;
            $element->save();

            // Show an info message
            Message::addInfo($GLOBALS['TL_LANG']['send-form-data-via-email-using-fz-hostings']['cannotActiveBothOptions']);

            $dc->reload();
        }
    }
}
