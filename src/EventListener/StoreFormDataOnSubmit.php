<?php

namespace Formatz\FzHostingsBundle\EventListener;

use Contao\CoreBundle\DependencyInjection\Attribute\AsHook;
use Contao\CoreBundle\Monolog\ContaoContext;
use Contao\Form;
use Formatz\FzHostingsBundle\Model\FormSubmissionModel;
use Psr\Log\LoggerInterface;

#[AsHook('processFormData')]
class StoreFormDataOnSubmit
{
    public function __construct(private readonly LoggerInterface $logger)
    {
    }

    public function __invoke(
        array $submittedData,
        array $formData,
        ?array $files,
        array $labels,
        Form $form,
    ): void {
        try {
            // Store the submitted data
            $formSubmission = new FormSubmissionModel();
            $formSubmission->form_id = $form->id;
            $formSubmission->data = json_encode($submittedData);
            $formSubmission->tstamp = time();
            $formSubmission->save();
        } catch (\Exception $e) {
            $this->logger->error(
                'Error while storing form submission: '.$e->getMessage(),
                ['contao' => new ContaoContext(__METHOD__, ContaoContext::ERROR)]
            );
        }
    }
}
