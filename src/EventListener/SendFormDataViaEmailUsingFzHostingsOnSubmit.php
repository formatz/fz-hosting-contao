<?php

namespace Formatz\FzHostingsBundle\EventListener;

use Contao\CoreBundle\DependencyInjection\Attribute\AsHook;
use Contao\CoreBundle\Monolog\ContaoContext;
use Contao\Form;
use Formatz\FzHostingsBundle\FzHostings\HostNotificationSender;
use Formatz\FzHostingsBundle\Model\FormFileModel;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Uid\UuidV4;

#[AsHook('prepareFormData')]
class SendFormDataViaEmailUsingFzHostingsOnSubmit
{
    public const FILES_DIR = '/var/tmp/';

    private string $filesDir;

    public function __construct(
        private readonly LoggerInterface $logger,
        private readonly HostNotificationSender $hostNotificationSender,
        private readonly RouterInterface $router,
        private readonly string $projectDir,
    ) {
        $this->filesDir = $this->projectDir.self::FILES_DIR;
    }

    public function __invoke(
        array $submittedData,
        array $labels,
        array $fields,
        Form $form,
        ?array $files = null,
    ): void {
        if ($form->sendFormDataViaEmailUsingFzHostings) {
            try {
                $filesModels = [];
                $filesUrls = [];

                // For Contao 4.13 and below, the files aren't passed as an argument
                if (empty($files) && !empty($_SESSION['FILES'])) {
                    $files = $_SESSION['FILES'];
                }

                if (!empty($files)) {
                    foreach ($files as $fieldName => $file) {
                        $filesModels[$fieldName] = $this->handleFileUploads($form, $file);
                        $filesUrls[$fieldName] = $this->generateUrl($filesModels[$fieldName]);
                    }

                    $submittedData = array_merge($submittedData, $filesUrls);
                }

                // Put to lower case the keys of the submitted data
                $submittedData = array_change_key_case($submittedData);

                $response = $this->hostNotificationSender->sendFormDataViaEmailUsingFzHostings([
                    'form' => $form->title,
                    'recipient' => $form->recipient,
                    'data' => $submittedData,
                    'labels' => $labels,
                    'date' => date('Y-m-d H:i:s'),
                    'referer' => $_SERVER['HTTP_REFERER'],
                    'userAgent' => $_SERVER['HTTP_USER_AGENT'],
                    'clientIp' => $_SERVER['REMOTE_ADDR'],
                    'clientDomain' => $_SERVER['SERVER_NAME'],
                ]);

                $form->sendViaEmail = false;

                // If the response is not null, it means that the email was sent
                if (null !== $response) {
                    return;
                }

                // Cannot add error on form for Contao 4.13 and below
                if (method_exists($form, 'addError')) {
                    $form->addError($GLOBALS['TL_LANG']['send-form-data-via-email-using-fz-hostings']['errorWhileSendingFormData']);
                }

                // If the email wasn't sent, delete the files
                foreach ($filesModels as $file) {
                    if (file_exists($file->file)) {
                        unlink($file->file);
                    }

                    $file->delete();
                }
            } catch (\Exception $e) {
                $this->logger->error(
                    'Error while sending form data: '.$e->getMessage(),
                    ['contao' => new ContaoContext(__METHOD__, ContaoContext::ERROR)]
                );
            }
        }
    }

    private function handleFileUploads(Form $form, array $file): FormFileModel
    {
        $formFile = new FormFileModel();
        $formFile->tstamp = time();
        $formFile->form_id = $form->id;
        $formFile->originalName = $file['name'];
        $formFile->name = (new UuidV4())->jsonSerialize().'-'.$file['name'];
        $formFile->expirationDate = (new \DateTime('now + 90 day'))->getTimestamp();
        $formFile->file = $this->filesDir.$formFile->name;

        $uploadedFile = new UploadedFile(
            $file['tmp_name'],
            $formFile->originalName,
            $file['type'],
            $file['error']
        );

        $uploadedFile->move($this->filesDir, $formFile->name);
        $formFile->save();

        return $formFile;
    }

    private function generateUrl(FormFileModel $file): string
    {
        return '<a href="'.$this->router->generate('form_files', [
            'name' => $file->name,
        ], RouterInterface::ABSOLUTE_URL).'">'.$file->originalName.'</a>';
    }
}
