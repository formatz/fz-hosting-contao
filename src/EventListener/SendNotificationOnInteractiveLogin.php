<?php

namespace Formatz\FzHostingsBundle\EventListener;

use Contao\BackendUser;
use Formatz\FzHostingsBundle\FzHostings\HostNotificationSender;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\Security\Http\Event\LoginSuccessEvent;
use Symfony\Component\Security\Http\SecurityEvents;

#[AsEventListener(event: LoginSuccessEvent::class)]
// Contao 4.13 support
#[AsEventListener(event: SecurityEvents::INTERACTIVE_LOGIN)]
final class SendNotificationOnInteractiveLogin
{
    public function __construct(
        private HostNotificationSender $notificationSender,
        private PropertyAccessorInterface $propertyAccessor,
    ) {
    }

    public function __invoke($event): void
    {
        if ($this->propertyAccessor->isReadable($event, 'authenticationToken')) {
            $token = $event->getAuthenticationToken();
        } elseif ($this->propertyAccessor->isReadable($event, 'authenticatedToken')) {
            $token = $event->getAuthenticatedToken();
        } else {
            return;
        }

        $user = $token->getUser();

        if ($user instanceof BackendUser) {
            $this->notificationSender->sendNotification($user);
        }
    }
}
