# Contao bundle to connect Contao instances to FZ Hosting server

Install bundle
```bash
composer require formatz/fz-hosting-contao-bundle
```

Migrate database
```bash
vendor/bin/contao-console contao:migrate
```

Configure the bundle in your Contao settings

